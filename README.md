<p align="center">
  <img src="https://raw.githubusercontent.com/CSSColorVars/csscolorvars/master/src/assets/cssColorVars-dark.png" alt="CSS ColorVars Logo"/>
</p>
<p align="center"><b>CSSColorVars (code generation interactive tool), which defines colors with CSS variables, improves efficiency and performance in front-end web development.</b>
<br>
No longer depend on CSS preprocessors to create color variables, use dynamic CSS variables.
The CSSColorVars interactive tool helps you create your colors with CSS variables and use dynamic functions to apply lightness, darkness and transparency in the color variable.</p>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
